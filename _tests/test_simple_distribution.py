#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test for the generation and fit of a simple von Mises distribution
"""

import unittest as ut

import vonMisesMixtures as vonmises
import numpy as np

class TestSimplevonMises(ut.TestCase):
    
    """Test the simple distribution generation and fit."""
    
    # def test_fit_precision006(self):
    #     mu, kappa, N = 2, 5, 200
    #     x = np.random.vonmises(mu, kappa, N)
    #     x = np.load("2-5-200.npy")
    #     mu_, kappa_ = vonmises.pdfit(x)
    #     self.assertIsInstance(mu_, float)
    #     self.assertIsInstance(kappa_, float)
    #     mu_check = abs(2*(mu-mu_)/(mu+mu_))
    #     kappa_check = abs(2*(kappa-kappa_)/(kappa+kappa_))
    #     check = 2/np.sqrt(N)
    #     self.assertTrue(mu_check < check)
    #     self.assertTrue(kappa_check < check)

    def test_fit_precision006(self):
        mu, kappa, N = 2, 5, 200
        x = np.load("2-5-200.npy")
        mu_, kappa_ = vonmises.pdfit(x)
        self.assertIsInstance(mu_, float)
        self.assertIsInstance(kappa_, float)
        mu_check = 2.026
        kappa_check = 4.978
        
        self.assertTrue(mu_check < check)
        self.assertTrue(kappa_check < check)    
        
    def test_fit_precision002(self):        
        mu, kappa, N = 2, 5, 2000
        x = np.random.vonmises(mu, kappa, N)
        mu_, kappa_ = vonmises.pdfit(x)
        self.assertIsInstance(mu_, float)
        self.assertIsInstance(kappa_, float)
        mu_check = abs(2*(mu-mu_)/(mu+mu_))
        kappa_check = abs(2*(kappa-kappa_)/(kappa+kappa_))
        check = 2/np.sqrt(N)
        self.assertTrue(mu_check < check)
        self.assertTrue(kappa_check < check)
        
    def test_fit_precision0007(self):         
        mu, kappa, N = 2, 5, 20000
        x = np.random.vonmises(mu, kappa, N)
        mu_, kappa_ = vonmises.pdfit(x)
        self.assertIsInstance(mu_, float)
        self.assertIsInstance(kappa_, float)
        mu_check = abs(2*(mu-mu_)/(mu+mu_))
        kappa_check = abs(2*(kappa-kappa_)/(kappa+kappa_))
        check = 2/np.sqrt(N)
        self.assertTrue(mu_check < check)
        self.assertTrue(kappa_check < check)
