#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test for the generation and fit of a simple von Mises distribution
"""

import unittest as ut

import vonMisesMixtures as vonmises
import numpy as np

class TestSimplevonMises(ut.TestCase):
    
    """Test the mixture distribution generation and fit."""

    def test_generate_mixture_raises(self):
        N = 2000
        ps = [0.6,0.6]
        mus = [-1,2]
        kappas = [6,8]
        with self.assertRaises(ValueError):
            x = vonmises.generate_mixtures(p=ps, mus=mus, kappas=kappas, sample_size=N)
        
        ps = [0.6,0.4]
        mus = [-1,]
        kappas = [6,8]
        with self.assertRaises(AssertionError):
            x = vonmises.generate_mixtures(p=ps, mus=mus, kappas=kappas, sample_size=N)

        ps = [0.6,]
        mus = [-1,0]
        kappas = [6,8]
        with self.assertRaises(AssertionError):
            x = vonmises.generate_mixtures(p=ps, mus=mus, kappas=kappas, sample_size=N)

        ps = [0.6,0.4]
        mus = [-1,0]
        kappas = [6,]
        with self.assertRaises(AssertionError):
            x = vonmises.generate_mixtures(p=ps, mus=mus, kappas=kappas, sample_size=N)

    def _order_params(self, params):
        indx = np.argsort(params[0,:])
        params_ordered = np.zeros(params.shape)
        for i, idx in enumerate(reversed(indx)):
            params_ordered[:,i] = params[:,idx]
        return params_ordered
    
    def test_fit_mixture2(self):
        N = 2000
        ps = [0.6,0.4]
        mus = [-1,2]
        kappas = [6,8]
        x = vonmises.generate_mixtures(p=ps, mus=mus, kappas=kappas, sample_size=N)
        params = vonmises.mixture_pdfit(x,n=2)
        params = self._order_params(params)
        self.assertIsInstance(params, np.ndarray)
        self.assertEqual(params.shape, (3,2))
        for i in range(params.shape[1]):
            p_, mu_, kappa_ = params[:,i]
            p_check = abs(2*(ps[i]-p_)/(ps[i]+p_))
            mu_check = abs(2*(mus[i]-mu_)/(mus[i]+mu_))
            kappa_check = abs(2*(kappas[i]-kappa_)/(kappas[i]+kappa_))
            check = 1/np.sqrt(N)
            with self.subTest((p_, ps[i])):
                self.assertTrue(p_check < len(ps)*check)
            with self.subTest((mu_,mus[i])):
                self.assertTrue(mu_check < check)
            with self.subTest((kappa_,kappas[i])):
                self.assertTrue(kappa_check < check)
        
    def test_fit_mixture3(self):
        N = 5000
        ps = [0.5, 0.3, 0.2]
        mus = [-1, 0, 2]
        kappas = [6, 8, 7]
        x = vonmises.generate_mixtures(p=ps, mus=mus, kappas=kappas, sample_size=N)
        params = vonmises.mixture_pdfit(x,n=3)
        params = self._order_params(params)
        self.assertIsInstance(params, np.ndarray)
        self.assertEqual(params.shape, (3,3))
        for i in range(params.shape[1]):
            p_, mu_, kappa_ = params[:,i]
            p_check = abs(2*(ps[i]-p_)/(ps[i]+p_))
            mu_check = abs(2*(mus[i]-mu_)/(mus[i]+mu_))
            kappa_check = abs(2*(kappas[i]-kappa_)/(kappas[i]+kappa_))
            check = 1/np.sqrt(N)
            with self.subTest((p_, ps[i])):
                self.assertTrue(p_check < len(ps)*check)
            with self.subTest((mu_,mus[i])):
                self.assertTrue(mu_check < check)
            with self.subTest((kappa_,kappas[i])):
                self.assertTrue(kappa_check < check)
