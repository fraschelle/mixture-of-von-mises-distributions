#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import setuptools

from vonMisesMixtures import __version__

with open("README.md", "r") as file:
    long_description_ = file.read()

with open("requirements.txt", "r") as file:
    install_requires_ = file.read()
    install_requires_ = install_requires_.split("\n")
    
setuptools.setup(
    name="vonMisesMixtures",
    version=__version__,
    author="François Konschelle",
    author_email="via.issue@only.please",
    description="Fit a mixture of von Mises distributions",
    long_description=long_description_,
    long_description_content_type="text/markdown",
    license="GNU GENERAL PUBLIC LICENSE v.3",
    url="https://framagit.org/fraschelle/mixture-of-von-mises-distributions/",
    packages=setuptools.find_packages(),
    install_requires = install_requires_,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Intended Audience :: Science/Research",
        "Intended Audience :: Education",
        "Intended Audience :: Financial and Insurance Industry",
        "Topic :: Education",
        "Topic :: Scientific/Engineering :: Mathematics",
        "Topic :: Scientific/Engineering :: Physics",
        "Topic :: Scientific/Engineering :: Astronomy",
        "Topic :: Scientific/Engineering",        
    ],
    python_requires='>=3.5',
)
