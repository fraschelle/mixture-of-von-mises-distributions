 #!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Calculate and fits some periodic von Mises distribution functions. 

All the functions below can be called directly from the main package name. That is

```python
import vonMisesMixtures as vm

vm.density(x, mu, kapp)
vm.mixtures.density(x, mu, kappa)
```
return the same things, and the same is true for all the functions below.
"""

from typing import Tuple 

import numpy as np
from scipy.special import iv
from scipy.optimize import fsolve

def vonmises_density(x: np.array, mu: np.array, kappa: np.array) -> np.array:
    """Alias for `density` function. Kept for retro-compatibility"""
    return density(x, mu, kappa)

def density(x: np.array, mu: np.array, kappa: np.array) -> np.array:
    """
    Calculate the von Mises density for a series x (a 1D numpy.array).
    
    Input | Type | Details
    -- | -- | --
    x | a 1D numpy.array of size L |
    mu | a 1D numpy.array of size n | the mean of the von Mises distributions
    kappa | a 1D numpy.array of size n | the dispersion of the von Mises distributions
    
    Output : 
        a (L x n) numpy array, L is the length of the series, and n is the size of the array containing the parameters. Each row of the output corresponds to a density
    """    
    not_normalized_density = np.array([np.exp(kappa*np.cos(i-mu)) for i in x])
    norm = 2*np.pi*iv(0,kappa)
    _density = not_normalized_density/norm
    return _density

def pdfit(series: np.array) -> Tuple[float]:
    """
    Calculate the estimator of the mean and deviation of a sample, for a von Mises distribution
    
    Input : 
        series : a 1D numpy.array
        
    Output : 
        the estimators of the parameters mu and kappa of a von Mises distribution, in a tuple (mu, kappa)
    See https://en.wikipedia.org/wiki/Von_Mises_distribution 
    for more details on the von Mises distribution and its parameters mu and kappa.
    """
    s0 = np.mean(np.sin(series))
    c0 = np.mean(np.cos(series))
    mu = np.arctan2(s0,c0)
    var = 1-np.sqrt(s0**2+c0**2)
    k = lambda kappa: 1-iv(1,kappa)/iv(0,kappa)-var
    kappa = fsolve(k, 0.0)[0]
    return mu, kappa 

def vonmises_pdfit(series: np.array) -> Tuple[float]:
    """Alias for `pdfit` function. Kept for retro-compatibility."""
    return pdfit(series)

def mixture_pdfit(series: np.array, n: int=2, threshold: float=1e-3) -> np.array:
    """
    Find the parameters of a mixture of von Mises distributions, using an EM algorithm.
    
    Input | Type | Details
    -- | -- | --
    series | a 1D numpy array | represent the stochastic periodic process
    n | an int | the number of von Mises distributions in the mixture
    threshold | a float | correspond to the euclidean distance between the old parameters and the new ones
    
    Output : a (3 x n) numpy-array, containing the probability amplitude of the distribution, 
    and the mu and kappa parameters on each line.
    """
    # initialise the parameters and the distributions
    pi = np.random.random(n)
    mu = np.random.vonmises(0.0, 0.0, n)
    kappa = np.random.random(n)
    t = pi*vonmises_density(series, mu, kappa)
    s = np.sum(t, axis=1)
    t = (t.T/s).T
    thresh = 1.0
    # calculate and update the coefficients, untill convergence
    while thresh > threshold:
        new_pi = np.mean(t, axis=0)
        new_mu = np.arctan2(np.sin(series)@t, np.cos(series)@t)      
        c = np.cos(series)@(t*np.cos(new_mu)) + np.sin(series)@(t*np.sin(new_mu))
        k = lambda kappa: (c-iv(1, kappa)/iv(0, kappa)*np.sum(t, axis=0)).reshape(n)
        new_kappa = fsolve(k, np.zeros(n))
        thresh = np.sum((pi-new_pi)**2 + (mu-new_mu)**2 + (kappa-new_kappa)**2)
        pi = new_pi
        mu = new_mu
        kappa = new_kappa
        t = pi*vonmises_density(series,mu,kappa)
        s = np.sum(t, axis=1)
        t = (t.T/s).T
    res = np.array([pi, mu, kappa])
    # in case there is no mixture, one fits the data using the estimators
    if n == 1:
        res = vonmises_pdfit(series)
        res = np.append(1.0,res)
        res = res.reshape(3,1)
    return res

def mixture_vonmises_pdfit(series: np.array, n: int=2, threshold: float=1e-3) -> np.array:
    """Alias for `mixture_pdfit` function. Kept for retro-compatibility."""
    return mixture_pdfit(series, n=n, threshold=threshold)
