# Mixture of von Mises disttributions

This module contains an EM algorithm aiming at fitting a mixture of [von-Mises distributions](https://en.wikipedia.org/wiki/Von_Mises_distribution).

 - Official repository is on [https://framagit.org/fraschelle/mixture-of-von-mises-distributions](https://framagit.org/fraschelle/mixture-of-von-mises-distributions)
 - Documentation is on [https://fraschelle.frama.io/mixture-of-von-mises-distributions/](https://fraschelle.frama.io/mixture-of-von-mises-distributions/)
 - PIP package is available on 

To use this package, first clone it, and then install it on your local machine: 

```bash
git clone https://framagit.org/fraschelle/mixture-of-von-mises-distributions.git
cd mixture-of-von-mises-distributions/
pip install .
```

or install it directly from the Python Package Installer (PyPi - pip) : 
```bash
pip install vonMisesMixtures
```

## Basic usage of the Package

Once installed, one can use the package as follow

```python
import vonMisesMixtures as vonmises
import numpy as np
import matplotlib.pyplot as plt

# Generate a random series of von Mises distributions
# Calculate the coefficients of the mixture using the EM algorithm

# change the coefficients at will
x = vonmises.tools.generate_mixtures(p=[0.3,0.7], # probability weigth of each sub-distribution
                                     mus=[0,2.5], # mu parameter of each sub-distribution
                                     kappas=[4,5], # kappa parameter of each sub-distribution
                                     sample_size=2500, # number of sample datas
                                     )
                                    

# calculate the coefficients
m = vonmises.mixture_pdfit(x,n=2)

# format of the array : 
# [
# [p1, p2, ...] # probability coefficients of the mixtures
# [mu1, mu2, ...] # mu coefficients of each sub-distribution
# [kappa1, kappa2, ...] # kappa coefficients of each sub-distribution
# ]

# plot the empirical distribution
x_histo = vonmises.tools.histogram(x,bins=25)
p1 = plt.plot(x_histo[0], x_histo[1], label='raw')

# plot the distribution using the parameters obtained from the EM algorithm
f = np.zeros(len(x_histo[0]))
for i in range(m.shape[1]):
    f += m[0,i]*vonmises.density(x_histo[0], m[1,i],m[2,i])
p2 = plt.plot(x_histo[0],f/np.sum(f), label='fit')

# display the two plots on the same figure
plt.legend()
plt.show()

```

## About us

Package developed in 2019 in order to fit periodic datas coming from time-frequency analysis / Fourier analysis on time series.

Thanks to Raphael Poix and [Adrien Todeschini](https://github.com/adrtod) for their helps.

Thank you very much for any suggestion or improvment that you may propose as issue on the [offocial repository](https://framagit.org/fraschelle/mixture-of-von-mises-distributions/-/issues).
