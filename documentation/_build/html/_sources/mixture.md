# vonMisesMixtures.mixtures

Calculate and fits some periodic von Mises distribution functions. 

All the functions below can be called directly from the main package name. That is

```python
import vonMisesMixtures as vm

vm.density(x, mu, kapp)
vm.mixtures.density(x, mu, kappa)
```
return the same things, and the same is true for all the functions below.

## vonmises\_density

```python
vonmises_density(x: np.array, mu: np.array, kappa: np.array) -> np.array
```

Alias for `density` function. Kept for retro-compatibility

## density

```python
density(x: np.array, mu: np.array, kappa: np.array) -> np.array
```

Calculate the von Mises density for a series x (a 1D numpy.array).

Input | Type | Details
-- | -- | --
x | a 1D numpy.array of size L |
mu | a 1D numpy.array of size n | the mean of the von Mises distributions
kappa | a 1D numpy.array of size n | the dispersion of the von Mises distributions

Output : 
    a (L x n) numpy array, L is the length of the series, and n is the size of the array containing the parameters. Each row of the output corresponds to a density

## pdfit

```python
pdfit(series: np.array) -> Tuple[float]
```

Calculate the estimator of the mean and deviation of a sample, for a von Mises distribution

Input : 
    series : a 1D numpy.array

Output : 
    the estimators of the parameters mu and kappa of a von Mises distribution, in a tuple (mu, kappa)
See https://en.wikipedia.org/wiki/Von_Mises_distribution 
for more details on the von Mises distribution and its parameters mu and kappa.

## vonmises\_pdfit

```python
vonmises_pdfit(series: np.array) -> Tuple[float]
```

Alias for `pdfit` function. Kept for retro-compatibility.

## mixture\_pdfit

```python
mixture_pdfit(series: np.array, n: int = 2, threshold: float = 1e-3) -> np.array
```

Find the parameters of a mixture of von Mises distributions, using an EM algorithm.

Input | Type | Details
-- | -- | --
series | a 1D numpy array | represent the stochastic periodic process
n | an int | the number of von Mises distributions in the mixture
threshold | a float | correspond to the euclidean distance between the old parameters and the new ones

Output : a (3 x n) numpy-array, containing the probability amplitude of the distribution, 
and the mu and kappa parameters on each line.

## mixture\_vonmises\_pdfit

```python
mixture_vonmises_pdfit(series: np.array, n: int = 2, threshold: float = 1e-3) -> np.array
```

Alias for `mixture_pdfit` function. Kept for retro-compatibility.

