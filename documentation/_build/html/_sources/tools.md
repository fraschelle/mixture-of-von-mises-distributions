# vonMisesMixtures.tools

Tools to 
 - substract a linear background from a von Mises distribution, 
 - generate a histogram from a random sample, 
 - generate an artificial mixtures of von Mises distributions,
 - calculate the Hellinger distance between two probability density functions.

## histogram

```python
histogram(series, bins=50)
```

Construct the histogram from the random series. The function associates
the middle of the bin coordinates to its normalized frequency.

Input | Type | Details
-- | -- | --
series | list or numpy.array | a random sample
bins  | int | the number of points in the outcome

Output : 
    (x, y) tuple, two numpy 1D arrays with y[i] the frequency and x[i] its 
    center coordinate

## least\_square\_periodic

```python
least_square_periodic(x, y)
```

Apply the least square method for a linear fit of a sample, periodic in the
y variable with period `2*numpy.pi`. Returns the a and b parameters of the
fit `y = a*x + b`

Input | Type | Details
-- | -- | --
`x` | a 1D `numpy.array` of size L |
`y` | a 1D numpy.array of size L |

Output : 
    (a, b) tuple, two float numbers with `y_fit = a*x + b`

## hellinger\_dist

```python
hellinger_dist(x1, x2)
```

Hellinger distance between the two samples distribution histogram
of same size x1 and x2.
See [wiki:Hellinger_distance](https://en.wikipedia.org/wiki/Hellinger_distance) for definitions.

Input : x1, x2 : lists or numpy 1D arrays

Output : A float number

## generate\_mixtures

```python
generate_mixtures(p=[0.5,0.5], mus=[0,3.14], kappas=[2,5], sample_size=100)
```

Generate a sample of size `sample_size` of several von Mises distributions
(the number of distribution is `len(p)==len(mus)==len(kappas)`).

 Input | Type | Details
 -- | -- | --
 p | a list of floating number summing to 1 | each float number represent the relative probability of each von Mises Sub-Distribution (vMSD)
 mus | a list of floats | The parameter mu of each of the vMSD.
 kappas | a list of floats | The parameter kappa of each of the vMSD
 sample_size | an int | the number of points in the random sample


Output : an array of size `sample_size`

