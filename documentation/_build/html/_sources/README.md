# Mixture of von Mises disttributions

This module contains an EM algorithm aiming at fitting a mixture of [von-Mises distributions](https://en.wikipedia.org/wiki/Von_Mises_distribution).

 - Official repository is on [https://framagit.org/fraschelle/mixture-of-von-mises-distributions](https://framagit.org/fraschelle/mixture-of-von-mises-distributions)
 - Documentation is on [https://fraschelle.frama.io/mixture-of-von-mises-distributions/](https://fraschelle.frama.io/mixture-of-von-mises-distributions/)
 - PIP package is available on [https://pypi.org/project/iamtokenizing/vonMisesMixtures](https://pypi.org/project/iamtokenizing/vonMisesMixtures)
 
Thank you very much for any suggestion or improvment that you may propose as issue on the [offocial repository](https://framagit.org/fraschelle/mixture-of-von-mises-distributions/-/issues).

