#!/usr/bin/env python
# coding: utf-8

# # Mixtures of von Mises distributions and the expectation maximization problem for periodic datas
# 
# When one deals with periodic stochastic phenomena, the von Mises distribution plays a central role. Indeed, it ressembles the normal distribution, but takes values in the circle. It generally gives a nice description of a peaked distribution around a given value in a periodic context, and the maximum likelihood estimators of its parameters are quite easy to compute (see below). 
# 
# In this note, we would like to extend this problem to a sample distribution that would be periodic, but present two or more peaks. The natural questions arising then is: how to compute the mixture components of a hypothetical mixture of von Mises distributions? 
# 
# ## von Mises distribution
# 
# A [von Mises probability distribution](https://en.wikipedia.org/wiki/Von_Mises_distribution) is characterized by its probability density
# 
# $$
# \left(\theta,\mu,\kappa\right)=\frac{e^{\kappa\cos\left(\theta-\mu\right)}}{2\pi I_{0}\left(\kappa\right)}
# $$
# 
# with $\kappa$ a scalling parameter, and $\mu$ being the expectation value of the distribution.
# 
# This distribution is defined on the circle $-\pi\leq\theta\leq\pi$, and 
# 
# $$
# I_{0}\left(\kappa\right)=\frac{1}{\pi}\int_{0}^{\pi}e^{\kappa\cos\theta}d\theta
# $$
# 
# is the modified Bessel function.

# ## Maximum likelihood estimators of $\mu$ and $\kappa$
# 
# Given a random sample $\left[x_{i}\right]_{i=1..N}$ of size $N$, one may want to fit this sample using a von Mises distribution. In that case, the maximum likelihood estimators $\hat{\mu}$ and $\hat{\kappa}$ of the von Mises distributions parameters $\mu$ and $\kappa$ can be found using the relations
# 
# \begin{align}
# \bar{C} = \frac{1}{N}\sum_{i=1}^{N}\cos x_{i} \\
# \bar{S} = \frac{1}{N}\sum_{i=1}^{N}\sin x_{i} \\
# \bar{\sigma} = 1-\sqrt{\bar{C}^{2}+\bar{S}^{2}}
# \end{align}
# 
# where $\bar{\sigma}$ is sometimes called the circular variance of the distribution. The estimators are then calculated using 
# 
# $$
# \tan\hat{\mu} = \frac{\bar{S}}{\bar{C}}
# $$
# and
# 
# $$
# \frac{I_{1}\left(\hat{\kappa}\right)}{I_{0}\left(\hat{\kappa}\right)}+\bar{\sigma}=1
# $$
# 
# this later relation being transcendental, one has to solve it numerically.

# ## Problem at hand
# 
# We suppose the complete distribution probability we would like to fit is
#  a mixture of two von Mises distributions of the form
# 
# $$
# g\left(x,\Phi\right)=pf\left(x,\mu_{1},\kappa_{1}\right)+\left(1-p\right)f\left(x,\mu_{2},\kappa_{2}\right)
# $$
# 
# where $\Phi=\left\{ p,\mu_{1,2},\kappa_{1,2}\right\} $ represents the set af all parameters characterizing the distribution. We here solve the [expectation-maximization algorithm](https://en.wikipedia.org/wiki/Expectation%E2%80%93maximization_algorithm) for this problem.

# ## Expectation-Maximization algorithm
# 
# 
# Define the log-likelihood function of the distribution given the data $\left\{ x_{i}\right\} _{i=1}^{n}$ : 
#  
# $$
# L\left(x;\Phi\right)=\sum_{i=1}^{n}\log\left[g\left(x_{i},\Phi\right)\right]
# $$
# 
# and we complete the statistics by adding a hidden variable $z_{i}$ to each $x_{i}$ able to classify the $x_{i}$'s. Because we have two distributions in the $g$-mixture, there are two $z_{i}$ : either $z_{i}=1$ when the data point $x_{i}$ belongs to $f\left(x,\mu_{1},\kappa_{1}\right)$ or $z_{i}=2$ when $x_{i}$ belongs to $f\left(x,\mu_{2},\kappa_{2}\right)$. One next construct a complete conditional log-likelihood of the form
# 
# $$
# L\left(x,z;\Phi\right)=\sum_{i=1}^{n}\log\left[g\left(z_{i}\mid x_{i},\Phi\right)\right]+L\left(x;\Phi\right)\\
# \Rightarrow L\left(x;\Phi\right)=L\left(x,z;\Phi\right)-\sum_{i=1}^{n}\log\left[g\left(z_{i}\mid x_{i},\Phi\right)\right]
# $$
# 
# so maximizing the log-likelihood of the initial problem reduces to a conditional problem on the hidden variable $z_{i}$. Taking the conditinal expectation of the previous line, parameterized by the parameters $\Phi^{\left(c\right)}$ at step $c$, one gets
# 
# \begin{align}
# \mathbb{E}\left[L\left(x;\Phi\right)\mid\Phi^{\left(c\right)}\right] & =\mathbb{E}\left[L\left(x,z;\Phi\right)\mid\Phi^{\left(c\right)}\right]-\mathbb{E}\left[\sum_{i=1}^{n}\log\left[g\left(z_{i}\mid x_{i},\Phi\right)\right]\right]\nonumber \\
# L\left(x;\Phi\right) & =Q\left(\Phi;\Phi^{\left(c\right)}\right)-\mathbb{E}\left[\sum_{i=1}^{n}\log\left[g\left(z_{i}\mid x_{i},\Phi\right)\right]\right]
# \end{align}
# 
# (since $L\left(x;\Phi\right)$ does not depend on $z$) and one can prove that 
# 
# $$
# \Phi^{\left(c+1\right)}=\arg\max_{\Phi}Q\left(\Phi;\Phi^{\left(c\right)}\right)
# $$
# 
# lead to a local maximum of $L\left(x;\Phi^{\left(c+1\right)}\right)$. So one can iteratively go to a local maximum of the log-likelihood for the distribution.

# ## Derivation of the algorithm for the mixture of von Mises distributions
# 
# We go back to the problem of the mixture of von Mises distributions. The complete log-likelihood reads
# 
# $$
# L\left(x,z;\Phi\right)=\sum_{i=1}^{n}z_{i1}\log\left[pf\left(x,\mu_{1},\kappa_{1}\right)\right]+z_{i2}\log\left[\left(1-p\right)f\left(x,\mu_{2},\kappa_{2}\right)\right]
# $$
# 
# with $z_{i1,2}=1$ if $x_{i}$ belongs to the distribution $1$ (resp. 2), and $z_{i1,2}=0$ otherwise.
#  
# The one has
# 
# $$
# Q\left(\Phi;\Phi^{\left(c\right)}\right)=\sum_{i=1}^{n}t_{i1}\log\left[pf\left(x,\mu_{1},\kappa_{1}\right)\right]+t_{i2}\log\left[\left(1-p\right)f\left(x,\mu_{2},\kappa_{2}\right)\right]
# $$
# 
# with 
# 
# $$
# t_{i1}=\mathbb{E}\left[z_{ik}\mid x,\Phi^{\left(c\right)}\right]=\frac{pf\left(x,\mu_{1},\kappa_{1}\right)}{pf\left(x,\mu_{1},\kappa_{1}\right)+\left(1-p\right)f\left(x,\mu_{2},\kappa_{2}\right)}
# $$
# 
# from Bayes rule, and the same for $t_{i2}$.
# 
# One has now to maximize $Q\left(\Phi;\Phi^{\left(c\right)}\right)$ to get the parameters at step $c+1$. This is done by calculating
# 
# \begin{align}
# \frac{\partial Q\left(\Phi;\Phi^{\left(c\right)}\right)}{\partial\mu_{1}}=\sum_{i=1}^{n}t_{i1}\kappa_{1}\sin\left(x_{i}-\mu_{1}\right)=0\\
# \Rightarrow\tan\mu_{1}^{\left(c+1\right)}=\frac{\sum_{i=1}^{n}t_{i1}^{\left(c\right)}\sin x_{i}}{\sum_{i=1}^{n}t_{i1}^{\left(c\right)}\cos x_{i}}
# \end{align}
# 
# and the same for $\mu_{2}$ (replacing $t_{i1}$ by $t_{i2}$).
# The other parameters are updated in order to verify
# 
# \begin{align}
# \frac{\partial Q\left(\Phi;\Phi^{\left(c\right)}\right)}{\partial\kappa_{1}}=\sum_{i=1}^{n}t_{i1}\cos\left(x_{i}-\mu_{1}\right)-\frac{I_{1}\left(\kappa_{1}\right)}{I_{0}\left(\kappa_{1}\right)}\sum_{i=1}^{n}t_{i1}=0\\
# \Rightarrow\frac{I_{1}\left(\kappa_{1}^{\left(c+1\right)}\right)}{I_{0}\left(\kappa_{1}^{\left(c+1\right)}\right)}=\frac{\sum_{i=1}^{n}t_{i1}^{\left(c\right)}\cos\left(x_{i}-\mu_{1}^{\left(c+1\right)}\right)}{\sum_{i=1}^{n}t_{i1}^{\left(c\right)}}
# \end{align}
# 
# 
# where the last equation must be solved numerically, since $I_{0,1}$ are tabulated functions. One more time, the update expression for $\kappa_{2}$ is the same as above, replacing $t_{i1}$ by $t_{i2}$.
# 
# So, at each step of the algorithm, the $t_i$ are first evaluated for all the sample points, and then the parameters $\mu_{i}$ and $\kappa_{i}$ are updated, before evaluating again the $t_i$, ...

# ## Generalization to several components of the mixture
# 
# Note to conclude that the generalization to the mixture of any number of von Mises distribution is trivial. Suppose one has 
# 
# $$
# g\left(x,\Phi\right)=\sum_{i=1}^{G}p_{i}f\left(x,\mu_{i},\kappa_{i}\right)
# $$
# 
# representing the mixture of $G$ von Mises distributions. Then it is clear that the calculation for the $t_{ik}$ would simply be
# 
# $$
# t_{ik}=\frac{p_{k}f\left(x,\mu_{k},\kappa_{k}\right)}{\sum_{k=1}^{G}p_{k}f\left(x,\mu_{k},\kappa_{k}\right)}
# $$
# 
# fo all $k\in{1,2,\cdots,G}$. Since one must have $\sum_{i=1}^{G}p_{i}=1$ the case of two components we detailled above is recovered instantaneously.

# In[1]:


from datetime import datetime
print("Last modification {}".format(datetime.now().strftime("%c")))

