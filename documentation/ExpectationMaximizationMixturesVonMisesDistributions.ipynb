{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Mixtures of von Mises distributions and the expectation maximization problem for periodic datas\n",
    "\n",
    "When one deals with periodic stochastic phenomena, the von Mises distribution plays a central role. Indeed, it ressembles the normal distribution, but takes values in the circle. It generally gives a nice description of a peaked distribution around a given value in a periodic context, and the maximum likelihood estimators of its parameters are quite easy to compute (see below). \n",
    "\n",
    "In this note, we would like to extend this problem to a sample distribution that would be periodic, but present two or more peaks. The natural questions arising then is: how to compute the mixture components of a hypothetical mixture of von Mises distributions? \n",
    "\n",
    "## von Mises distribution\n",
    "\n",
    "A [von Mises probability distribution](https://en.wikipedia.org/wiki/Von_Mises_distribution) is characterized by its probability density\n",
    "\n",
    "$$\n",
    "\\left(\\theta,\\mu,\\kappa\\right)=\\frac{e^{\\kappa\\cos\\left(\\theta-\\mu\\right)}}{2\\pi I_{0}\\left(\\kappa\\right)}\n",
    "$$\n",
    "\n",
    "with $\\kappa$ a scalling parameter, and $\\mu$ being the expectation value of the distribution.\n",
    "\n",
    "This distribution is defined on the circle $-\\pi\\leq\\theta\\leq\\pi$, and \n",
    "\n",
    "$$\n",
    "I_{0}\\left(\\kappa\\right)=\\frac{1}{\\pi}\\int_{0}^{\\pi}e^{\\kappa\\cos\\theta}d\\theta\n",
    "$$\n",
    "\n",
    "is the modified Bessel function."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Maximum likelihood estimators of $\\mu$ and $\\kappa$\n",
    "\n",
    "Given a random sample $\\left[x_{i}\\right]_{i=1..N}$ of size $N$, one may want to fit this sample using a von Mises distribution. In that case, the maximum likelihood estimators $\\hat{\\mu}$ and $\\hat{\\kappa}$ of the von Mises distributions parameters $\\mu$ and $\\kappa$ can be found using the relations\n",
    "\n",
    "\\begin{align}\n",
    "\\bar{C} = \\frac{1}{N}\\sum_{i=1}^{N}\\cos x_{i} \\\\\n",
    "\\bar{S} = \\frac{1}{N}\\sum_{i=1}^{N}\\sin x_{i} \\\\\n",
    "\\bar{\\sigma} = 1-\\sqrt{\\bar{C}^{2}+\\bar{S}^{2}}\n",
    "\\end{align}\n",
    "\n",
    "where $\\bar{\\sigma}$ is sometimes called the circular variance of the distribution. The estimators are then calculated using \n",
    "\n",
    "$$\n",
    "\\tan\\hat{\\mu} = \\frac{\\bar{S}}{\\bar{C}}\n",
    "$$\n",
    "and\n",
    "\n",
    "$$\n",
    "\\frac{I_{1}\\left(\\hat{\\kappa}\\right)}{I_{0}\\left(\\hat{\\kappa}\\right)}+\\bar{\\sigma}=1\n",
    "$$\n",
    "\n",
    "this later relation being transcendental, one has to solve it numerically."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Problem at hand\n",
    "\n",
    "We suppose the complete distribution probability we would like to fit is\n",
    " a mixture of two von Mises distributions of the form\n",
    "\n",
    "$$\n",
    "g\\left(x,\\Phi\\right)=pf\\left(x,\\mu_{1},\\kappa_{1}\\right)+\\left(1-p\\right)f\\left(x,\\mu_{2},\\kappa_{2}\\right)\n",
    "$$\n",
    "\n",
    "where $\\Phi=\\left\\{ p,\\mu_{1,2},\\kappa_{1,2}\\right\\} $ represents the set af all parameters characterizing the distribution. We here solve the [expectation-maximization algorithm](https://en.wikipedia.org/wiki/Expectation%E2%80%93maximization_algorithm) for this problem."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Expectation-Maximization algorithm\n",
    "\n",
    "\n",
    "Define the log-likelihood function of the distribution given the data $\\left\\{ x_{i}\\right\\} _{i=1}^{n}$ : \n",
    " \n",
    "$$\n",
    "L\\left(x;\\Phi\\right)=\\sum_{i=1}^{n}\\log\\left[g\\left(x_{i},\\Phi\\right)\\right]\n",
    "$$\n",
    "\n",
    "and we complete the statistics by adding a hidden variable $z_{i}$ to each $x_{i}$ able to classify the $x_{i}$'s. Because we have two distributions in the $g$-mixture, there are two $z_{i}$ : either $z_{i}=1$ when the data point $x_{i}$ belongs to $f\\left(x,\\mu_{1},\\kappa_{1}\\right)$ or $z_{i}=2$ when $x_{i}$ belongs to $f\\left(x,\\mu_{2},\\kappa_{2}\\right)$. One next construct a complete conditional log-likelihood of the form\n",
    "\n",
    "$$\n",
    "L\\left(x,z;\\Phi\\right)=\\sum_{i=1}^{n}\\log\\left[g\\left(z_{i}\\mid x_{i},\\Phi\\right)\\right]+L\\left(x;\\Phi\\right)\\\\\n",
    "\\Rightarrow L\\left(x;\\Phi\\right)=L\\left(x,z;\\Phi\\right)-\\sum_{i=1}^{n}\\log\\left[g\\left(z_{i}\\mid x_{i},\\Phi\\right)\\right]\n",
    "$$\n",
    "\n",
    "so maximizing the log-likelihood of the initial problem reduces to a conditional problem on the hidden variable $z_{i}$. Taking the conditinal expectation of the previous line, parameterized by the parameters $\\Phi^{\\left(c\\right)}$ at step $c$, one gets\n",
    "\n",
    "\\begin{align}\n",
    "\\mathbb{E}\\left[L\\left(x;\\Phi\\right)\\mid\\Phi^{\\left(c\\right)}\\right] & =\\mathbb{E}\\left[L\\left(x,z;\\Phi\\right)\\mid\\Phi^{\\left(c\\right)}\\right]-\\mathbb{E}\\left[\\sum_{i=1}^{n}\\log\\left[g\\left(z_{i}\\mid x_{i},\\Phi\\right)\\right]\\right]\\nonumber \\\\\n",
    "L\\left(x;\\Phi\\right) & =Q\\left(\\Phi;\\Phi^{\\left(c\\right)}\\right)-\\mathbb{E}\\left[\\sum_{i=1}^{n}\\log\\left[g\\left(z_{i}\\mid x_{i},\\Phi\\right)\\right]\\right]\n",
    "\\end{align}\n",
    "\n",
    "(since $L\\left(x;\\Phi\\right)$ does not depend on $z$) and one can prove that \n",
    "\n",
    "$$\n",
    "\\Phi^{\\left(c+1\\right)}=\\arg\\max_{\\Phi}Q\\left(\\Phi;\\Phi^{\\left(c\\right)}\\right)\n",
    "$$\n",
    "\n",
    "lead to a local maximum of $L\\left(x;\\Phi^{\\left(c+1\\right)}\\right)$. So one can iteratively go to a local maximum of the log-likelihood for the distribution."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Derivation of the algorithm for the mixture of von Mises distributions\n",
    "\n",
    "We go back to the problem of the mixture of von Mises distributions. The complete log-likelihood reads\n",
    "\n",
    "$$\n",
    "L\\left(x,z;\\Phi\\right)=\\sum_{i=1}^{n}z_{i1}\\log\\left[pf\\left(x,\\mu_{1},\\kappa_{1}\\right)\\right]+z_{i2}\\log\\left[\\left(1-p\\right)f\\left(x,\\mu_{2},\\kappa_{2}\\right)\\right]\n",
    "$$\n",
    "\n",
    "with $z_{i1,2}=1$ if $x_{i}$ belongs to the distribution $1$ (resp. 2), and $z_{i1,2}=0$ otherwise.\n",
    " \n",
    "The one has\n",
    "\n",
    "$$\n",
    "Q\\left(\\Phi;\\Phi^{\\left(c\\right)}\\right)=\\sum_{i=1}^{n}t_{i1}\\log\\left[pf\\left(x,\\mu_{1},\\kappa_{1}\\right)\\right]+t_{i2}\\log\\left[\\left(1-p\\right)f\\left(x,\\mu_{2},\\kappa_{2}\\right)\\right]\n",
    "$$\n",
    "\n",
    "with \n",
    "\n",
    "$$\n",
    "t_{i1}=\\mathbb{E}\\left[z_{ik}\\mid x,\\Phi^{\\left(c\\right)}\\right]=\\frac{pf\\left(x,\\mu_{1},\\kappa_{1}\\right)}{pf\\left(x,\\mu_{1},\\kappa_{1}\\right)+\\left(1-p\\right)f\\left(x,\\mu_{2},\\kappa_{2}\\right)}\n",
    "$$\n",
    "\n",
    "from Bayes rule, and the same for $t_{i2}$.\n",
    "\n",
    "One has now to maximize $Q\\left(\\Phi;\\Phi^{\\left(c\\right)}\\right)$ to get the parameters at step $c+1$. This is done by calculating\n",
    "\n",
    "\\begin{align}\n",
    "\\frac{\\partial Q\\left(\\Phi;\\Phi^{\\left(c\\right)}\\right)}{\\partial\\mu_{1}}=\\sum_{i=1}^{n}t_{i1}\\kappa_{1}\\sin\\left(x_{i}-\\mu_{1}\\right)=0\\\\\n",
    "\\Rightarrow\\tan\\mu_{1}^{\\left(c+1\\right)}=\\frac{\\sum_{i=1}^{n}t_{i1}^{\\left(c\\right)}\\sin x_{i}}{\\sum_{i=1}^{n}t_{i1}^{\\left(c\\right)}\\cos x_{i}}\n",
    "\\end{align}\n",
    "\n",
    "and the same for $\\mu_{2}$ (replacing $t_{i1}$ by $t_{i2}$).\n",
    "The other parameters are updated in order to verify\n",
    "\n",
    "\\begin{align}\n",
    "\\frac{\\partial Q\\left(\\Phi;\\Phi^{\\left(c\\right)}\\right)}{\\partial\\kappa_{1}}=\\sum_{i=1}^{n}t_{i1}\\cos\\left(x_{i}-\\mu_{1}\\right)-\\frac{I_{1}\\left(\\kappa_{1}\\right)}{I_{0}\\left(\\kappa_{1}\\right)}\\sum_{i=1}^{n}t_{i1}=0\\\\\n",
    "\\Rightarrow\\frac{I_{1}\\left(\\kappa_{1}^{\\left(c+1\\right)}\\right)}{I_{0}\\left(\\kappa_{1}^{\\left(c+1\\right)}\\right)}=\\frac{\\sum_{i=1}^{n}t_{i1}^{\\left(c\\right)}\\cos\\left(x_{i}-\\mu_{1}^{\\left(c+1\\right)}\\right)}{\\sum_{i=1}^{n}t_{i1}^{\\left(c\\right)}}\n",
    "\\end{align}\n",
    "\n",
    "\n",
    "where the last equation must be solved numerically, since $I_{0,1}$ are tabulated functions. One more time, the update expression for $\\kappa_{2}$ is the same as above, replacing $t_{i1}$ by $t_{i2}$.\n",
    "\n",
    "So, at each step of the algorithm, the $t_i$ are first evaluated for all the sample points, and then the parameters $\\mu_{i}$ and $\\kappa_{i}$ are updated, before evaluating again the $t_i$, ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Generalization to several components of the mixture\n",
    "\n",
    "Note to conclude that the generalization to the mixture of any number of von Mises distribution is trivial. Suppose one has \n",
    "\n",
    "$$\n",
    "g\\left(x,\\Phi\\right)=\\sum_{i=1}^{G}p_{i}f\\left(x,\\mu_{i},\\kappa_{i}\\right)\n",
    "$$\n",
    "\n",
    "representing the mixture of $G$ von Mises distributions. Then it is clear that the calculation for the $t_{ik}$ would simply be\n",
    "\n",
    "$$\n",
    "t_{ik}=\\frac{p_{k}f\\left(x,\\mu_{k},\\kappa_{k}\\right)}{\\sum_{k=1}^{G}p_{k}f\\left(x,\\mu_{k},\\kappa_{k}\\right)}\n",
    "$$\n",
    "\n",
    "fo all $k\\in{1,2,\\cdots,G}$. Since one must have $\\sum_{i=1}^{G}p_{i}=1$ the case of two components we detailled above is recovered instantaneously."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Last modification Tue Oct  5 20:54:34 2021\n"
     ]
    }
   ],
   "source": [
    "from datetime import datetime\n",
    "print(\"Last modification {}\".format(datetime.now().strftime(\"%c\")))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
